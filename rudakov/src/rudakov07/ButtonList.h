/**
 * @file ButtonList.h
 * Part of Lab7
 * @author User
 * @version 0.0.1
 * @date 2019.01.01
 */

#ifndef BUTTONLIST_H_
#define BUTTONLIST_H_

#include <iostream>

#include "Button.h"


class ButtonList
{
	Button ** button;

	int size;

public:

	ButtonList() :size(0), button(NULL) {};

	~ButtonList() {
		delete[] button;
	};

	void addButton(Button *princ);


	void removeButton();


	void showAll();

	void getButton(int index);

	int getSize() const;
	/**Метод записывает все данные о мониторах в файл
	*/
	void saveAllToFiles();
	//void fwriteAll();
	/**Метод считывает все данные о мониторах из файла
	*/
	void loadFromFiles();
	//void freadAll();

	int findButtons(bool isPressed);

	Button* operator[](int index);
};

///Class Functor
class FunctorButton{
public:
	template<typename T>
	void operator()(T& stList, bool isPressed){
		int res = stList.findButtons(isPressed);
	}
};

#endif /* BUTTONLIST_H_ */
