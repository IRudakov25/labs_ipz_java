/**
 * @file ButtonList.cpp
 * Part of Lab7
 * @author User
 * @version 0.0.1
 * @date 2019.01.01
 */

#include "ButtonList.h"

#include <iostream>
#include <ctime>
#include <string>
#include <fstream>

using std::cout;
using std::string;
using std::endl;
using std::ofstream;
using std::ifstream;

void ButtonList::addButton(Button *princ) {
	Button **newData = new Button*[size + 1];
	for (int i = 0; i < size; i++) {
		for (int i = 0; i < size; i++) {
			newData[i] = button[i];
		}

	}
	if (size != 0) {
		delete[] button;
	}
	button = newData;
	button[size] = princ;
	size++;
}

void ButtonList::removeButton() {
	Button ** newData = new Button* [size - 1];

	for (int i = 0; i < size - 1; i++) {
		newData[i] = button[i];
	}


	delete[] button;

	button = newData;

	size--;
}

int stroki() {
	ifstream fpin("text.txt");
	string str;
	int k = 0;
	while (getline(fpin, str)) {
		k++;
	}
	fpin.close();
	return k;
}

void ButtonList::showAll()
{
	for (int i = 1; i <= size; i++) {
		getButton(i);
	}
}


void ButtonList::getButton(int index) {
	for (auto i = 1; i <= size; i++) {
		if (i == index) {
			cout << "# " << i << " "<<endl;
			cout << "Button shape: " << button[i - 1]->getShape() << endl;
			cout << "Is button pressed?: " << button[i - 1]->getButton() << endl;
		}
	}

}

int ButtonList::getSize() const
{
	return size;
}

typedef ofstream outf;

void ButtonList::saveAllToFiles(){
	for(int i = 0; i < size; i++){
		std::ofstream out; // поток для записи
		std::string fileName("Human");
		int radix = 10;  //система счисления
		char buffer[20]; //результат
		char *p;  //указатель на результат
		p = itoa(i,buffer,radix);
		fileName += p;
		fileName += ".txt";
		out.open(fileName); // окрываем файл для записи
		if (out.is_open()){
			out << this->button[i]->getShape() << std::endl
				<< this->button[i]->getButton() << std::endl;
		}
		else {
			std::cout << "Error!" << std::endl;
		}
		out.close();
	}
}

void ButtonList::loadFromFiles(){
	std::string line;
	for(int i = 0;i < this->size; i++){
		std::string fileName("Buttons");
		int radix = 10;
		char buffer[20];
		char *p;
		p = itoa(i,buffer,radix);
		fileName += p;
		fileName += ".txt";
		std::ifstream in(fileName); // окрываем файл для чтения
		if (in.is_open()){

			bool isPr[1];
			//int buff[];
			//enum buff[];
			//int i = 0;
			while (getline(in, line)){

				isPr[i] = std::stoi(line);
				i++;
			}
			//Button* wh = new Button(buff[0], buff[1]);
			Button* wh = new Button(isPr[1]);
		//	wh->setShape(Shape);
			this->addButton(wh);
		} else {
			//std::cout << "Error while open file! -> " << fileName << std::endl;
			break;
		}
		in.close();
	}
}

Button* ButtonList::operator[](int index) {
	if (index > size) {
		cout << "Error";
		return nullptr;
	}
	else {
		return button[index];
	}
}

int ButtonList::findButtons(bool isPressed) {
	int res = 0;
		for(int i = 0; i<this->getSize();i++){
			if(button[i]->getButton() == isPressed){
				this->getButton(i+1);
				res++;
			}
		}
		return res;
}


