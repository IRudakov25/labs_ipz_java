/**
 * @file Screen.cpp
 * Part of Lab4
 * @author Rudakov I., KIT-27b
 * @version 0.0.1
 * @date 2019.10.14
 */

#include "Screen.h"

Screen::Screen() {
	// TODO Auto-generated constructor stub

}

Screen::~Screen() {
	// TODO Auto-generated destructor stub
}

void Screen::ShowHeader(){
	std::cout << "\u0389" << "\u0388"<< "\u0391"<< "\u0394"<< "\u0388"<< "\u01A6\n";
}
void Screen::ShowContent(const Button& iButton){
	if (iButton.getButton() == 0){
		std::cout << iButton.getButton() << " - this means that button is unpressed" << std::endl;
		}else{
		std::cout << iButton.getButton() << " - this means that button is pressed" << std::endl;
		}
		if (iButton.getShape() == RECTANGULAR){
			std::cout << iButton.getShape() << " - this means that button has rectangular shape" << std::endl;
		}else if (iButton.getShape() == OVAL){
			std::cout << iButton.getShape() << " - this means that button has oval shape" << std::endl;
		}
}
void Screen::ShowContent(const KeyboardButton& iKeyboardButton){}
void Screen::ShowFooter(){
	std::cout << "\u0191" << "\u01A0"<< "\u01A0"<< "\u01AC"<< "\u018E"<< "\u01A6\n";
}


