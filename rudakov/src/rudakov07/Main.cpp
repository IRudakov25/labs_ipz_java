/**
 * @file Main.cpp
 * Part of Lab7
 * @author Rudakov I., KIT-27b
 * @version 0.0.1
 * @date 2019.10.14
 */

#include "Button.h"
#include "Screen.h"
#include "GraphScreen.h"
#include "KeyboardButton.h"
#include "BaseView.h"
#include "KeyboardButtonScreen.h"
#include "FileStorage.h"
#include "ButtonList.h"

int main(){
	Button button;
	ButtonList list;
	Button *buttonRecPres = new Button();
		buttonRecPres->setButton(true);
		buttonRecPres->setShape(RECTANGULAR);

		Button *buttonRecUnPres = new Button();
		buttonRecUnPres->setButton(false);
		buttonRecUnPres->setShape(RECTANGULAR);

		Button *buttonOvPres = new Button();
		buttonOvPres->setButton(true);
		buttonOvPres->setShape(OVAL);

		Button *buttonOvUnPres = new Button();
		buttonOvUnPres->setButton(false);
		buttonOvUnPres->setShape(OVAL);

		KeyboardButton *keyboardButton = new KeyboardButton();
		keyboardButton->setButton(false);
		keyboardButton->setShape(RECTANGULAR);
		keyboardButton->setSerialNumber(int('B'));
		keyboardButton->setButtonName("B");

		list.addButton(buttonOvPres);
		list.addButton(buttonOvUnPres);
		list.addButton(buttonRecPres);
		list.addButton(buttonRecUnPres);
		list.showAll();
		std::cout << "____________________________" << std::endl;
		list.removeButton();
		list.showAll();
		std::cout << "____________________________" << std::endl;

		FunctorButton fb;
		fb(list,1);

		delete buttonRecPres;
		delete buttonRecUnPres;
		delete buttonOvPres;
		delete buttonOvUnPres;
		delete keyboardButton;
	return 0;
}
