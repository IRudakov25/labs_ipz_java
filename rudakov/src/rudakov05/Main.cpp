/**
 * @file Main.cpp
 * Part of Lab5
 * @author Rudakov I., KIT-27b
 * @version 0.0.1
 * @date 2019.10.14
 */

#include "Button.h"
#include "Screen.h"
#include "GraphScreen.h"
#include "KeyboardButton.h"
#include "BaseView.h"
#include "KeyboardButtonScreen.h"
#include "FileStorage.h"
#include "ButtonList.h"

int main() {

	Button button;
	button.setButton(1);
	button.setShape(RECTANGULAR);
	BaseView *bV = new Screen();
	bV->Display(button);
	CFileStorage *store = CFileStorage::Create(button, "lab05");
	bool ok = store->Store();
	if (ok) {
		std::cout << "true" << std::endl;
	} else {
		std::cout << "false" << std::endl;
	}
	delete bV;
	return 0;
}
