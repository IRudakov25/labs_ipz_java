/**
 * @file KeyboardButton.h
 * Part of Lab3
 * @author Rudakov I., KIT-27b
 * @version 0.0.1
 * @date 2019.10.09
 */

#ifndef KEYBOARDBUTTON_H_
#define KEYBOARDBUTTON_H_
#include "Button.h"
#include <string>

/**
 * This class is a child for Button
 */
class KeyboardButton: public Button {
public:
	KeyboardButton();
	virtual ~KeyboardButton();

	/**
	 * Setter for keyboardButton`s serial number
	 * @param aSerialNumber is for transmitting data
	 */
	void setSerialNumber(int aSerialNumber);
	/**
	 * Setter for keyboardButton`s name
	 * @param sButtonName is for transmitting data
	 */
	void setButtonName(std::string sButtonName);

	/**
	 * Getter for keyboardButton`s serial number
	 */
	int getSerialNumber() const;
	/**
	 * Getter for keyboardButton`s name
	 */
	std::string getButtonName() const;
private:
	int serialNumber;
	std::string buttonName;
};

#endif /* KEYBOARDBUTTON_H_ */
