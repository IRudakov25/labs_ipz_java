/**
 * @file KeyboardButton.cpp
 * Part of Lab3
 * @author Rudakov I., KIT-27b
 * @version 0.0.1
 * @date 2019.10.09
 */

#include "KeyboardButton.h"

KeyboardButton::KeyboardButton() {
	// TODO Auto-generated constructor stub

}

KeyboardButton::~KeyboardButton() {
	// TODO Auto-generated destructor stub
}

void KeyboardButton::setSerialNumber(int aSerialNumber){
	serialNumber = aSerialNumber;
}
void KeyboardButton::setButtonName(std::string sButtonName){
	buttonName = sButtonName;
}

int KeyboardButton::getSerialNumber() const{
	return serialNumber;
}
std::string KeyboardButton::getButtonName() const{
	return buttonName;
}

