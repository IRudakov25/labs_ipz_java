/**
 * @file Screen.h
 * Part of Lab3
 * @author Rudakov I., KIT-27b
 * @version 0.0.1
 * @date 2019.10.09
 */

#ifndef SCREEN_H_
#define SCREEN_H_
#include "Button.h"
#include "BaseView.h"
#include <iostream>

/**
 * This is class-child of BaseView
 */
class Screen: public BaseView {
public:
	Screen();
	virtual ~Screen();

	/**
	 * The function of the specific header for Screen
	 */
	void ShowHeader() override;
	/**
	 * The function of the specific content for Screen
	 * @param iButton needs for sending data
	 */
	void ShowContent(const Button& iButton) override;
	/**
	 * The function does not override there
	 * @param iKeyboardButton needs for sending data
	 */
	void ShowContent(const KeyboardButton& iKeyoardButton) override;
	/**
	 * The function of the specific footer for Screen
	 */
	void ShowFooter() override;
};

#endif /* SCREEN_H_ */
