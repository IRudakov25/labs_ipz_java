/**
 * @file BaseView.cpp
 * Part of Lab3
 * @author Rudakov I., KIT-27b
 * @version 0.0.1
 * @date 2019.10.09
 */

#include "BaseView.h"

BaseView::BaseView() {
	// TODO Auto-generated constructor stub

}

BaseView::~BaseView() {
	// TODO Auto-generated destructor stub
}
void BaseView::Display(const Button& iButton){
	ShowHeader();
	ShowContent(iButton);
	ShowFooter();
}
void BaseView::Display(const KeyboardButton& iKeyBoardButton){
	ShowHeader();
	ShowContent(iKeyBoardButton);
	ShowFooter();
}



