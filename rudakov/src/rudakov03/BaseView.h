/**
 * @file BaseView.h
 * Part of Lab3
 * @author Rudakov I., KIT-27b
 * @version 0.0.1
 * @date 2019.10.09
 */

#ifndef BASEVIEW_H_
#define BASEVIEW_H_
#include "Button.h"
#include "KeyboardButton.h"

/**
 * This is the parent class for Screen, GraphScreen and KeyboardButtonScreen classes
 */
class BaseView {
public:
	BaseView();
	virtual ~BaseView();
	Button* iButton = new Button();
	KeyboardButton* iKeyboardButton = new KeyboardButton();
	/**
	 * This function was created to Display <View1> and <View2> classes
	 * @param iButton needs for sending data
	 */
	void Display(const Button& iButton);
	/**
	 * This function was created to Display <View3> class
	 * @param iKeyBoardButton needs for sending data
	 */
	void Display(const KeyboardButton& iKeyBoardButton);
protected:
	/**
	 * The function of the header
	 */
	virtual void ShowHeader() = 0;
	/**
	 * The function of the content for <View1> and <View2> classes
	 * @param iButton needs for sending data
	 */
	virtual void ShowContent(const Button& iButton) = 0;
	/**
	 * The function of the content for <View3> class
	 * @param iKeyboardButton needs for sending data
	 */
	virtual void ShowContent(const KeyboardButton& iKeyboardButton) = 0;
	/**
	 * The function of the footer
	 */
	virtual void ShowFooter() = 0;
};

#endif /* BASEVIEW_H_ */
