/**
 * @file KeyboardButtonScreen.h
 * Part of Lab3
 * @author Rudakov I., KIT-27b
 * @version 0.0.1
 * @date 2019.10.09
 */

#ifndef KEYBOARDBUTTONSCREEN_H_
#define KEYBOARDBUTTONSCREEN_H_
#include "BaseView.h"
#include "KeyboardButton.h"

/**
 * This is class child of BaseView
 */
class KeyboardButtonScreen: public BaseView {
public:
	KeyboardButtonScreen();
	virtual ~KeyboardButtonScreen();
	KeyboardButton* iKeybooardButton = new KeyboardButton();

	/**
	 * The function of the specific header for KeyboardButtonScreen
	 */
	void ShowHeader() override;
	/**
	 * The function does not override there
	 * @param iButton needs for sending data
	 */
	void ShowContent(const Button& iButton) override;
	/**
	 * The function of the specific content for KeyboardButtonScreen
	 * @param iKeyboardButton needs for sending data
	 */
	void ShowContent(const KeyboardButton& iKeyboardButton) override;
	/**
	 * The function of the specific footer for KeyboardButtonScreen
	 */
	void ShowFooter() override;
};

#endif /* KEYBOARDBUTTONSCREEN_H_ */
