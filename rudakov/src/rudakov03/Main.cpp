/**
 * @file Main.cpp
 * Part of Lab3
 * @author Rudakov I., KIT-27b
 * @version 0.0.1
 * @date 2019.10.09
 */

#include "Button.h"
#include "Screen.h"
#include "GraphScreen.h"
#include "KeyboardButton.h"
#include "BaseView.h"
#include "KeyboardButtonScreen.h"


void isButtonPressedMethod(Button *button, Button *buttonOvPres,
		Button *buttonOvUnPres, Button *buttonRecPres,
		Button *buttonRecUnPres) {

	std::cout << "Is this button Round and pressed? - " << button->IsRoundPressed(button) << std::endl;
	std::cout << "Is this button Round and pressed? - " << button->IsRoundPressed(buttonOvPres) << std::endl;
	std::cout << "Is this button Round and pressed? - " << button->IsRoundPressed(buttonOvUnPres) << std::endl;
	std::cout << "Is this button Round and pressed? - " << button->IsRoundPressed(buttonRecPres) << std::endl;
	std::cout << "Is this button Round and pressed? - " << button->IsRoundPressed(buttonRecUnPres) << std::endl;
}

int main(){

	Button *button = new Button();
	Screen *screen = new Screen();

	Button *buttonRecPres = new Button();
	buttonRecPres->setButton(true);
	buttonRecPres->setShape(RECTANGULAR);

	Button *buttonRecUnPres = new Button();
	buttonRecUnPres->setButton(false);
	buttonRecUnPres->setShape(RECTANGULAR);

	Button *buttonOvPres = new Button();
	buttonOvPres->setButton(true);
	buttonOvPres->setShape(OVAL);

	Button *buttonOvUnPres = new Button();
	buttonOvUnPres->setButton(false);
	buttonOvUnPres->setShape(OVAL);

	KeyboardButton *keyboardButton = new KeyboardButton();
	keyboardButton->setButton(false);
	keyboardButton->setShape(RECTANGULAR);
	keyboardButton->setSerialNumber(int('M'));
	keyboardButton->setButtonName("M");


	BaseView *baseViewScreen = new Screen();
	std::cout << "Show the <View1> class with <Data1> data using <BaseView>:\n";
	baseViewScreen->Display(*buttonOvPres);
	BaseView *baseViewGraphScreen = new GraphScreen();
	std::cout << "Show the <View2> class with <Data1> data using <BaseView>:\n";
	baseViewGraphScreen->Display(*buttonOvPres);
	BaseView *baseViewKeyboardButtonScreen = new KeyboardButtonScreen();
	std::cout << "Show the <View3> class with <Data2> data using <BaseView>:\n";
	baseViewKeyboardButtonScreen->Display(*keyboardButton);

	delete button;
	delete screen;
	delete buttonRecPres;
	delete buttonRecUnPres;
	delete buttonOvPres;
	delete buttonOvUnPres;
	delete baseViewScreen;
	delete baseViewGraphScreen;
	delete baseViewKeyboardButtonScreen;
	delete keyboardButton;
	return 0;
}
