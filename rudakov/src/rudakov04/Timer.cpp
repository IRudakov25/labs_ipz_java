/**
 * @file Timer.cpp
 * Part of Lab4
 * @author Rudakov I., KIT-27b
 * @version 0.0.1
 * @date 2019.10.14
 */
#include "Timer.h"


Timer::Timer(int time){
	this->setTime(time);
	this->setReTime(1);
}

Timer::~Timer(){
}

void Timer::start(){

	for(int i = 0; i < getTime(); i++){

		std::cout << "Time remaning: " << getTime() - i << std::endl;
		Sleep(1000);
		}
	restart();



}

void Timer::restart(){
	retime--;
	if(retime > 0){
		start();
	} else {
		std::cout << "End" << std::endl;
	}
}

int Timer::getTime(){
	return time;
}
void Timer::setTime(int t){
	this->time = t;
}
int Timer::getReTime(){
	return retime;
}
void Timer::setReTime(int t){
	this->retime = t;
}

