/**
 * @file Button.h
 * Part of Lab4
 * @author Rudakov I., KIT-27b
 * @version 0.0.1
 * @date 2019.10.14
 */

#ifndef BUTTON_H_
#define BUTTON_H_
#include <iostream>

/**
 * Enumeration for Shape of Button
 */
enum Shape{
	DEFAULT,    //!< default
	RECTANGULAR,//!< rectangular shape
	OVAL        //!< oval (round) shape
};
/**
 * My main class, is a parent for KeyboardButton
 */
class Button {
private:
	Shape shape;
	bool isButtonPressed;
public:
	Button();
	virtual ~Button();

	/**
	 * Setter for button`s state
	 * @param isButtonDown is for sending data
	 */
	void setButton(bool isButtonDown);
	/**
	 * Setter for button`s shape
	 * @param shape1 is for sending data
	 */
	void setShape(Shape shape1);

	/**
	 * Getter for button`s state
	 * @return isButtonDown
	 */
	bool getButton() const;
	/**
	 * Getter for button`s shape
	 * @return shape
	 */
	Shape getShape() const;

	/**
	 * The function is for finding a specific button: shape must be
	 * round, the button must be pressed
	 * @param button is for transmitting data
	 * @return true if button is round and pressed
	 * @return false if button has other shape or state
	 */
	bool IsRoundPressed(Button* button) const;
};

#endif /* BUTTON_H_ */
