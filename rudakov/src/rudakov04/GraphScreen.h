/**
 * @file GraphScreen.h
 * Part of Lab4
 * @author Rudakov I., KIT-27b
 * @version 0.0.1
 * @date 2019.10.14
 */

#ifndef GRAPHSCREEN_H_
#define GRAPHSCREEN_H_
#include "Button.h"
#include "BaseView.h"

/**
 * Class-child from BaseView
 */
class GraphScreen: public BaseView {
public:
	GraphScreen();
	virtual ~GraphScreen();

	Button* iButton = new Button();

	/**
	 * The function of the specific header for GraphScreen
	 */
	void ShowHeader() override;
	/**
	 * The function of the specific content for GraphScreen
	 * @param iButton needs for sending data
	 */
	void ShowContent(const Button& iButton) override;
	/**
	 * The function does not override there
	 * @param iKeyboardButton needs for sending data
	 */
	void ShowContent(const KeyboardButton& iKeyboardButton) override;
	/**
	 * The function of the specific footer for GraphScreen
	 */
	void ShowFooter() override;
};

#endif /* GRAPHSCREEN_H_ */
