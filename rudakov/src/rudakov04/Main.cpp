/**
 * @file Main.cpp
 * Part of Lab4
 * @author Rudakov I., KIT-27b
 * @version 0.0.1
 * @date 2019.10.14
 */

#include "Button.h"
#include "Screen.h"
#include "GraphScreen.h"
#include "KeyboardButton.h"
#include "BaseView.h"
#include "KeyboardButtonScreen.h"


void isButtonPressedMethod(Button *button, Button *buttonOvPres,
		Button *buttonOvUnPres, Button *buttonRecPres,
		Button *buttonRecUnPres) {

	std::cout << "Is this button Round and pressed? - " << button->IsRoundPressed(button) << std::endl;
	std::cout << "Is this button Round and pressed? - " << button->IsRoundPressed(buttonOvPres) << std::endl;
	std::cout << "Is this button Round and pressed? - " << button->IsRoundPressed(buttonOvUnPres) << std::endl;
	std::cout << "Is this button Round and pressed? - " << button->IsRoundPressed(buttonRecPres) << std::endl;
	std::cout << "Is this button Round and pressed? - " << button->IsRoundPressed(buttonRecUnPres) << std::endl;
}

int main(){

	Button *button = new Button();
	Screen *screen = new Screen();

	Button *buttonRecPres = new Button();
	buttonRecPres->setButton(true);
	buttonRecPres->setShape(RECTANGULAR);

	Button *buttonRecUnPres = new Button();
	buttonRecUnPres->setButton(false);
	buttonRecUnPres->setShape(RECTANGULAR);

	Button *buttonOvPres = new Button();
	buttonOvPres->setButton(true);
	buttonOvPres->setShape(OVAL);

	Button *buttonOvUnPres = new Button();
	buttonOvUnPres->setButton(false);
	buttonOvUnPres->setShape(OVAL);

	KeyboardButton *keyboardButton = new KeyboardButton();
	keyboardButton->setButton(false);
	keyboardButton->setShape(RECTANGULAR);
	keyboardButton->setSerialNumber(int('B'));
	keyboardButton->setButtonName("B");


	BaseView *baseViewScreen = new Screen();
	baseViewScreen->OnTimerAction(4);
	std::cout << "Show the <View1> class with <Data1> data using <BaseView>:\n";
	baseViewScreen->Display(*buttonOvPres);

	BaseView *baseViewGraphScreen = new GraphScreen();
	baseViewGraphScreen->OnTimerAction(4);
	std::cout << "Show the <View2> class with <Data1> data using <BaseView>:\n";
	baseViewGraphScreen->Display(*buttonOvPres);


	BaseView *baseViewKeyboardButtonScreen = new KeyboardButtonScreen();
	baseViewKeyboardButtonScreen->OnTimerAction(4);
	std::cout << "Show the <View3> class with <Data2> data using <BaseView>:\n";
	baseViewKeyboardButtonScreen->Display(*keyboardButton);


	KeyboardButton *keyboardButton2 = new KeyboardButton();
	keyboardButton2->SetData(int('C'), "B");
	bool proverka = int('P')==keyboardButton2->getSerialNumber();
	std::cout << proverka << " - result\n";
	bool proverka1 = int('C')==keyboardButton2->getSerialNumber();
	std::cout << proverka1 << " - result\n";




//	keyboardButton2->SetData(1024);

	delete button;
	delete screen;
	delete buttonRecPres;
	delete buttonRecUnPres;
	delete buttonOvPres;
	delete buttonOvUnPres;
	delete baseViewScreen;
	delete baseViewGraphScreen;
	delete baseViewKeyboardButtonScreen;
	delete keyboardButton;
	delete keyboardButton2;
	return 0;
}
