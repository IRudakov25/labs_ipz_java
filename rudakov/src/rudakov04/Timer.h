/**
 * @file Timer.h
 * Part of Lab4
 * @author Rudakov I., KIT-27b
 * @version 0.0.1
 * @date 2019.10.14
 */
#ifndef SRC_TIMER_H_
#define SRC_TIMER_H_
#include "BaseView.h"

#include <iostream>
#include <windows.h>
#include <stdlib.h>
#include <cstdlib>


 ///Timer class

class Timer{
public:
	/**
	 * Function for starting timer
	 */
	void start();
	/**
	 * Function for restarting timer
	 */
	void restart();

	/**
	 * Constructor with parameter
	 *
	 * @param time
	 */
	Timer(int time);

	/**
	 * Destructor of the class
	 */
	virtual ~Timer();
	/**
	 * The getter for the time
	 */
	int getTime();
	/**
	 * The setter for the time
	 *
	 * @param t
	 */
	void setTime(int t);
	/**
	 * Getter for the retime
	 */
	int getReTime();
	/**
	 * Setter for the retime
	 *
	 * @param t
	 */
	void setReTime(int t);

private:
	/**
	 * Timer's time
	 */
	int time;

	/**
	 * Interval between recalls
	 */
	int retime;

};

#endif /* SRC_TIMER_H_ */


