/**
 * @file Screen.cpp
 * Part of Lab2
 * @author Rudakov I., KIT-27b
 * @version 0.0.1
 * @date 2019.09.27
 */

#include "Screen.h"

Screen::Screen() {
	// TODO Auto-generated constructor stub

}

Screen::~Screen() {
	// TODO Auto-generated destructor stub
}

Screen::Screen(const Screen &other) {
	// TODO Auto-generated constructor stub

}

Screen::Screen(Screen &&other) {
	// TODO Auto-generated constructor stub

}

void Screen::toPrint(Button* button) const{
	if (button->getButton() == 0){
	std::cout << button->getButton() << " - this means that button is unpressed" << std::endl;
	}else{
	std::cout << button->getButton() << " - this means that button is pressed" << std::endl;
	}
	if (button->getShape() == RECTANGULAR){
		std::cout << button->getShape() << " - this means that button has rectangular shape" << std::endl;
	}else if (button->getShape() == OVAL){
		std::cout << button->getShape() << " - this means that button has oval shape" << std::endl;
	}
}


