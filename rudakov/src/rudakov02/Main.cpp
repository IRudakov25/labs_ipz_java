/**
 * @file Main.cpp
 * Part of Lab2
 * @author Rudakov I., KIT-27b
 * @version 0.0.1
 * @date 2019.09.27
 */

#include "Button.h"
#include "Screen.h"
#include "GraphScreen.h"

#define SM 128

void isButtonPressedMethod(Button *button, Button *buttonOvPres,
		Button *buttonOvUnPres, Button *buttonRecPres,
		Button *buttonRecUnPres) {

	std::cout << "Is this button Round and pressed? - " << button->IsRoundPressed(button) << std::endl;
	std::cout << "Is this button Round and pressed? - " << button->IsRoundPressed(buttonOvPres) << std::endl;
	std::cout << "Is this button Round and pressed? - " << button->IsRoundPressed(buttonOvUnPres) << std::endl;
	std::cout << "Is this button Round and pressed? - " << button->IsRoundPressed(buttonRecPres) << std::endl;
	std::cout << "Is this button Round and pressed? - " << button->IsRoundPressed(buttonRecUnPres) << std::endl;
}

int main(){

	Button *button = new Button();
	Screen *screen = new Screen;
	/**
	 * You can choose a condition.
	 * true - the button is pressed
	 * false - the button is unpressed
	 */
	Button *buttonRecPres = new Button();
	buttonRecPres->setButton(true);
	/**
	 * You can choose a shape.
	 * RECTANGULAR - rectangular
	 * OVAL - oval
	*/
	buttonRecPres->setShape(RECTANGULAR);
	std::cout << "First Button" << std::endl;
	screen->toPrint(buttonRecPres);

	Button *buttonRecUnPres = new Button();
	buttonRecUnPres->setButton(false);
	buttonRecUnPres->setShape(RECTANGULAR);
	std::cout << "Second Button" << std::endl;
	screen->toPrint(buttonRecUnPres);

	Button *buttonOvPres = new Button();
	buttonOvPres->setButton(true);
	buttonOvPres->setShape(OVAL);
	std::cout << "Third Button" << std::endl;
	screen->toPrint(buttonOvPres);

	Button *buttonOvUnPres = new Button();
	buttonOvUnPres->setButton(false);
	buttonOvUnPres->setShape(OVAL);
	std::cout << "Fourth Button" << std::endl;
	screen->toPrint(buttonOvUnPres);


	isButtonPressedMethod(button, buttonOvPres, buttonOvUnPres, buttonRecPres,
			buttonRecUnPres);

	GraphScreen *graphScreen =  new GraphScreen();
	std::cout << "\u2554" << "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"
			<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2566" << "\u2550" << "\u2550"
			<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2557\n";
	std::cout << "\u2551" << "Is Button Pressed?" << "\u2551" << "Shape" << "\u2551\n";
	std::cout << "\u2560"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"
			<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2569"<< "\u2550"<< "\u2550"
			<< "\u2550"<< "\u2550" << "\u2550"<< "\u2563\n";
	graphScreen->PrintButton(*buttonOvPres);
	graphScreen->PrintButton(*buttonOvUnPres);
	graphScreen->PrintButton(*buttonRecPres);
	graphScreen->PrintButton(*buttonRecUnPres);
	std::cout << "\u2551" << "\u0020"<< "\u0020"<< "\u0020"<< "\u0020"
			<< "\u0020" << "\u0020"<< "\u0020"<< "THIS IS END" << "\u0020"
			<< "\u0020"<< "\u0020"<<  "\u0020"<< "\u0020"<< "\u0020"<< "\u2551\n";
	std::cout << "\u255A" << "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"
				<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550" << "\u2550" << "\u2550"
				<< "\u2550"<< "\u2550"<< "\u2550"<< "\u255D\n";

	delete button;
	delete screen;
	delete buttonRecPres;
	delete buttonRecUnPres;
	delete buttonOvPres;
	delete buttonOvUnPres;
	delete graphScreen;
	return 0;
}
