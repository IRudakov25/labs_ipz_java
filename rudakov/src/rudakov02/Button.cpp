/**
 * @file Button.cpp
 * Part of Lab2
 * @author Rudakov I., KIT-27b
 * @version 0.0.1
 * @date 2019.09.27
 */

#include "Button.h"

Button::Button() {
	// TODO Auto-generated constructor stub
	isButtonPressed = false;
	shape = DEFAULT;

}

Button::~Button() {
	// TODO Auto-generated destructor stub
}

Button::Button(const Button &other) {
	// TODO Auto-generated constructor stub

}

Button::Button(Button &&other) {
	// TODO Auto-generated constructor stub

}

Button::Button(bool isButtonDown, Shape shape1){
	isButtonPressed = isButtonDown;
	shape = shape1;
}

void Button::setButton(bool isButtonDown){
	isButtonPressed = isButtonDown;
}

void Button::setShape(Shape shape1){
	shape = shape1;
}

bool Button::getButton() const{
	return isButtonPressed;
}

Shape Button::getShape() const{
	return shape;
}

bool Button::IsRoundPressed(Button* button) const{
	if(button->getButton() == true && button->getShape() == OVAL)
		return true;
	else
		return false;
}


