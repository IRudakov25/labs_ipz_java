/**
 * @file GraphScreen.h
 * Part of Lab2
 * @author Rudakov I., KIT-27b
 * @version 0.0.1
 * @date 2019.09.27
 */

#ifndef GRAPHSCREEN_H_
#define GRAPHSCREEN_H_
#include "Button.h"

/**
 * Class for button`s displaying with Unicode symbols
 */
class GraphScreen {
public:
	GraphScreen();
	virtual ~GraphScreen();
	GraphScreen(const GraphScreen &other);
	GraphScreen(GraphScreen &&other);

	Button* iButton = new Button();

	void SetButtonSource(const Button* aButton);
	/**
	 * The function to display button with Unicode symbols
	 * @param iButton is for transmitting data
	 */
	void PrintButton(const Button& iButton);
};

#endif /* GRAPHSCREEN_H_ */
