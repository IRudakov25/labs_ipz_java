/**
 * @file Screen.h
 * Part of Lab2
 * @author Rudakov I., KIT-27b
 * @version 0.0.1
 * @date 2019.09.27
 */

#ifndef SCREEN_H_
#define SCREEN_H_
#include "Button.h"
#include <iostream>

/**
 * Class for simple button`s displaying
 */
class Screen {
public:
	Screen();
	virtual ~Screen();
	Screen(const Screen &other);
	Screen(Screen &&other);

	/**
	 * The function to display button with text
	 * @param button is for transmitting data
	 */
	void toPrint(Button* button) const;

};

#endif /* SCREEN_H_ */
