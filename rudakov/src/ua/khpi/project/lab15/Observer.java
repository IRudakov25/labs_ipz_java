package ua.khpi.project.lab15;

/**
 * Представляет метод для взаимодействия наблюдаемого объекта и наблюдателя;
 * шаблон Observer
 * 
 * @author Рудаков И.С., группа КИТ-117Г
 * @version 1.0
 * @see Observable
 */
public interface Observer {
	/**
	 * Вызывается наблюдаемым объектом для каждого наблюдателя; шаблон Observer
	 * 
	 * @param observable ссылка на наблюдаемый объект
	 * @param event      информация о событии
	 */
	public void handleEvent(Observable observable, Object event);
}