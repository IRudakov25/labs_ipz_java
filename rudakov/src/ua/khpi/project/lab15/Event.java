package ua.khpi.project.lab15;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Аннотация времени выполнения для назначения методам наблюдателя конкретных
 * событий
 * 
 * @author Рудаков И.С., группа КИТ-117Г
 * @see AnnotatedObserver
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Event {
	String value();
}