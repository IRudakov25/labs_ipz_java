package ua.khpi.project.lab14;

import ua.khpi.project.lab13.Command;

/**
 * Представляет методы для помещения и извлечения задач обработчиком потока;
 * шаблон Worker Thread
 * 
 * @author Рудаков И.С., группа КИТ-117Г
 * @version 1.0
 * @see Command
 */
public interface Queue {
	/**
	 * Добавляет новую задачу в очередь; шаблон Worker Thread
	 * 
	 * @param cmd задача
	 */
	void put(Command cmd);

	/**
	 * Удаляет задачу из очереди; шаблон Worker Thread
	 * 
	 * @return удаляемая задача
	 */
	Command take();
}
