package ua.khpi.project.lab14;

import ua.khpi.project.lab11.View;
import ua.khpi.project.lab11.ViewableResult;
import ua.khpi.project.lab13.ChangeConsoleCommand;
import ua.khpi.project.lab13.GenerateConsoleCommand;
import ua.khpi.project.lab13.Menu;
import ua.khpi.project.lab13.ViewConsoleCommand;

/**
 * Вычисление и отображение результатов; содержит реализацию статического метода
 * main()
 * 
 * @author Рудаков И.С., группа КИТ-117Г
 * @version 5.0
 * @see Main#main
 */
public class Main {
	/**
	 * Объект, реализующий интерфейс {@linkplain View}; обслуживает коллекцию
	 * объектов {@linkplain ua.khpi.project.lab10.Item2d}; инициализируется с помощью Factory Method
	 */
	private View view = new ViewableResult().getView();
	/**
	 * Объект класса {@linkplain Menu}; макрокоманда (шаблон Command)
	 */
	private Menu menu = new Menu();

	/** Обработка команд пользователя */
	public void run() {
		menu.add(new ViewConsoleCommand(view));
		menu.add(new GenerateConsoleCommand(view));
		menu.add(new ChangeConsoleCommand(view));
		menu.add(new ExecuteConsoleCommand(view));
		menu.execute();
	}

	/**
	 * Выполняется при запуске программы
	 * 
	 * @param args параметры запуска программы
	 */
	public static void main(String[] args) {
		Main main = new Main();
		main.run();
	}
}
