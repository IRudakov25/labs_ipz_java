package ua.khpi.project.lab11;

/** Creator
 * (шаблон проектирования
 * Factory Method) <br>
 * Объявляет метод
 * "фабрикующий" объекты
 * @author Рудаков И.С., КИТ-27Б
 * @version 1.0
 */

public interface Viewable {

	/**  Создает объект, реализующий {@linkplain View} */
	public View getView();
}
