package ua.khpi.project.lab11;
/** ConcreteCreator
 * (шаблон проектирования
 * Factory Method) <br>
 * Объявляет метод
 * "фабрикующий" объекты
 * @author Рудаков И.С., КИТ-27Б
 * @version 1.0
 * @see Viewable
 * @see ViewableResult#getView()
 */
public class ViewableResult implements Viewable {
	/** Создает отображаемый объект {@linkplain ViewResult} */
	@Override
	public View getView() {
		return new ViewResult();
	}
}
