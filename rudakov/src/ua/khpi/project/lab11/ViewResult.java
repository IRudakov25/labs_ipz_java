package ua.khpi.project.lab11;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import ua.khpi.project.lab10.Item2d;

/** ConcreteProduct
 * (шаблон проектирования
 * Factory Method) <br>
 * Вычисление функции, сохранение и
 * отображение результатов
 * @author Рудаков И.С., КИТ-27Б
 * @version 1.0
 * @see Viewable
 * @see ViewableResult#getView()
 */
public class ViewResult implements View {
	/** Имя файла, используемое при сериализации. */
	private static final String FNAME = "items.bin";
	
	/** Определяет количество значений для вычисления по умолчанию */
	private static final int DEFAULT_NUM = 10;
	
	/** Коллекция аргументов и результатов вычислений */
	private ArrayList<Item2d> items = new ArrayList<Item2d>();
	
	/** Вызывает {@linkplain ViewResult#ViewResult(int n) ViewResult(int n)}
	 * с параметром {@linkplain ViewResult#DEFAULT_NUM DEFAULT_NUM} 
	 */
	public ViewResult() {
		this(DEFAULT_NUM);
	}

	/** Инициализирует коллекцию {@linkplain ViewResult#items}
	 *  @param n начальное количество элементов
	 */
	public ViewResult(int n) {
		for(int ctr = 0; ctr < n; ctr++) {
			items.add(new Item2d());
		}
	}
	
	/** Получить значение {@linkplain ViewResult#items}
	 *  @return текущее значение ссылки на объект {@linkplain ArrayList}
	 */
	public ArrayList<Item2d> getItems() {
		return items;
	}
	
	/** Вычисляет значение функции
	 * @param weight аргумент вычисляемой функции
	 * @param g аргумент вычисляемой функции
	 * @param height аргумент вычисляемой функции
	 * @return результат вычисления функции
	 */
	public double calc(double weight, double g, double height) {
		return weight * g * height;
	}
	
	/** Вычисляет значение функции и сохраняет
	 * результат в коллекции {@linkplain ViewResult#items}
	 * @param weight аргумент вычисляемой функции
	 * @param g аргумент вычисляемой функции
	 * @param height аргумент вычисляемой функции
	 */
	public void init(double weight, double g, double height) {
		double w = 5.0;
		double h = 7.0;
		for (Item2d item : items) {
			item.setWeight(weight);
			item.setG(g);
			item.setHeight(height);
			item.setFormula(calc(weight, g, height));
			weight += w;
			height += h;
		}
	}
	
	/** Вызывает <b>init(double weight, double g, double height)</b>
	 * со случайным значением аргумента <br>
	 * {@inheritDoc} 
	 */
	@Override
	public void viewInit() {
		init((Math.random() * 100.0), 9.81, (Math.random() * 360.0));
	}
	
	/** Реализация метода {@linkplain View#viewSave() }<br>
	 * {@inheritDoc} 
	 */
	@Override
	public void viewSave() throws IOException{
		ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(FNAME));
		os.writeObject(items);
		os.flush();
		os.close();
	}
	
	/** Реализация метода {@linkplain View#viewRestore() }<br>
	 * {@inheritDoc} 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void viewRestore() throws Exception {
		ObjectInputStream is = new ObjectInputStream(new FileInputStream(FNAME));
		items = (ArrayList<Item2d>) is.readObject();
		is.close();
	}
	
	/** Реализация метода {@linkplain View#viewHeader() }<br>
	 * {@inheritDoc} 
	 */
	@Override
	public void viewHeader() {
		System.out.println("Results:\n");
	}
	
	/** Реализация метода {@linkplain View#viewBody() }<br>
	 * {@inheritDoc} 
	 */
	@Override
	public void viewBody() {
		for(Item2d item : items) {
			System.out.printf("(%.2f kilos; %.2f (metres^2)/sec.; %.2f metres; %.2f J.) ", item.getWeight(), item.getG(), item.getHeight(), item.getY());
			System.out.println("\tResult in binary: " + Long.toBinaryString(Double.doubleToRawLongBits(item.getY())) + "J.");
		}
		System.out.println();
	}
	
	/** Реализация метода {@linkplain View#viewFooter() }<br>
	 * {@inheritDoc} 
	 */
	@Override
	public void viewFooter() {
		System.out.println("End.");		
	}
	
	/** Реализация метода {@linkplain View#viewShow() }<br>
	 * {@inheritDoc} 
	 */
	@Override
	public void viewShow() {
		viewHeader();
		viewBody();
		viewFooter();
	}

}
