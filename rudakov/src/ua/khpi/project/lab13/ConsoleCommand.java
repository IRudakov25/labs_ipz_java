package ua.khpi.project.lab13;

/**
 * Интерфейс консольной команды; шаблон Command
 * 
 * @author Рудаков И.С., КИТ-27Б
 * @version 1.0
 */
public interface ConsoleCommand extends Command {
	/**
	 * Горячая клавиша команды; шаблон Command
	 * 
	 * @return символ горячей клавиши
	 */
	public char getKey();
}
