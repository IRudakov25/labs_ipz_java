package ua.khpi.project.lab13;

/**
 * Интерфейс команды или задачи; шаблоны: Command, Worker Thread
 * 
 * @author Рудаков И.С., КИТ-27Б
 * @version 1.0
 */
public interface Command {
	/** Выполнение команды; шаблоны: Command, Worker Thread */
	public void execute();
}