package ua.khpi.project.lab13;

import ua.khpi.project.lab10.Item2d;
import ua.khpi.project.lab11.View;
import ua.khpi.project.lab11.ViewResult;

/**
 * Консольная команда Change item; шаблон Command
 * 
 * @author Рудаков И.С., КИТ-27Б
 * @version 1.0
 */
public class ChangeConsoleCommand extends ChangeItemCommand implements ConsoleCommand {
	/**
	 * Объект, реализующий интерфейс {@linkplain View}; обслуживает коллекцию
	 * объектов {@linkplain ex01.Item2d}
	 */
	private View view;

	/**
	 * Возвращает поле {@linkplain ChangeConsoleCommand#view}
	 * 
	 * @return значение {@linkplain ChangeConsoleCommand#view}
	 */
	public View getView() {
		return view;
	}

	/**
	 * Устанавливает поле {@linkplain ChangeConsoleCommand#view}
	 * 
	 * @param view значение для {@linkplain ChangeConsoleCommand#view}
	 * @return новое значение {@linkplain ChangeConsoleCommand#view}
	 */
	public View setView(View view) {
		return this.view = view;
	}

	/**
	 * Инициализирует поле {@linkplain ChangeConsoleCommand#view}
	 * 
	 * @param view объект, реализующий интерфейс {@linkplain View}
	 */
	public ChangeConsoleCommand(View view) {
		this.view = view;
	}

	@Override
	public char getKey() {
		return 'c';
	}

	@Override
	public String toString() {
		return "'c'hange";
	}

	@Override
	public void execute() {
		System.out.printf("Change item: scale factor %.2f\n", setOffset(Math.random() * 100.0));

		for (Item2d item : ((ViewResult) view).getItems()) {
			super.setItem(item);
			super.execute();
		}

		view.viewShow();
	}
}
