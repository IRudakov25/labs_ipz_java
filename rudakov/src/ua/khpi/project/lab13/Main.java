package ua.khpi.project.lab13;
/** Вычисление и отображение
* результатов; cодержит реализацию
* статического метода main()
* @author Рудаков И.С., КИТ-27Б
* @version 4.0
* @see Main#main
*/
public class Main {
	public static void main(String[] args) {
		Application app = Application.getInstance();
		app.run(null);
		}
}