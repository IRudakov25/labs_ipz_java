package ua.khpi.project.lab12;
import java.util.Formatter;

import ua.khpi.project.lab10.Item2d;
import ua.khpi.project.lab11.ViewResult;
import ua.khpi.project.lab11.Viewable;
import ua.khpi.project.lab11.ViewableResult;

/** ConcreteProduct
 * (шаблон проектирования
 * Factory Method) <br>
 * Вычисление функции, сохранение и
 * отображение результатов в виде таблицы
 * @author Рудаков И.С., КИТ-27Б
 * @version 1.0
 * @see Viewable
 * @see ViewableResult#getView()
 */
public class ViewTable extends ViewResult {
	/** Определяет количество значений для вычисления по умолчанию */
	private static final int DEFAULT_WIDTH = 65;
	
	/** Текущая ширина таблицы */
	private int width;
	
	/** Устанавливает {@linkplain ViewTable#width_width}
	 * значением {@linkplain ViewTablet#DEFAULT_WIDTH DEFAULT_WIDTH} <br>
	 * Вызывается конструктор суперкласса {@linkplain ViewResult#ViewResult() ViewResult()} 
	 */
	public ViewTable() {
		width = DEFAULT_WIDTH;
	}

	/** Устанавливает {@linkplain ViewTable#width} значением <b>width</b><br> 
	 * Вызывается конструктор суперкласса {@linkplain ViewResult#ViewResult() ViewResult()}
	 *  @param width опредеяет ширину таблицы
	 */
	public ViewTable(int width) {
		this.width = width;
	}
	
	/** Устанавливает {@linkplain ViewTable#width} значением <b>width</b><br> 
	 * Вызывается конструктор суперкласса {@linkplain ViewResult#ViewResult(int n) ViewResult(int n)}
	 *  @param width опредеяет ширину таблицы
	 *  @param n количество элементов коллекции; передается суперконструктору
	 */
	public ViewTable(int width, int n) {
		super(n);
		this.width = width;
	}
	
	/**
	 * Устанавливает поле {@linkplain ViewTable#width} значчением <b>width</b>
	 * @param width новая ширина таблицы
	 * @return заданная параметром <b>width</b> ширина таблицы
	 */
	public int setWidth(int width) {
		return this.width = width;
	}
	
	/**
	 * Возвращает значение поля {@linkplain ViewTable#width}
	 * @return текущая ширина таблицы
	 */
	public int getWidth() {
		return width;
	}
	
	/** 
	 * Выводит вертикальный разделитель 
	 * шириной {@linkplain ViewTable#width} символов
	 */
	private void outLine() {
		for(int i = width+50; i > 0; i--) {
			System.out.print('-');
		}
	}
	
	/** 
	 * Вызывает {@linkplain ViewTable#outLine()}; 
	 * завершает вывод разделителем строки
	 */
	private void outLineLn() {
		outLine();
		System.out.println();
	}
	
	/**
	 * Выводит заголовок таблицы шириной {@linkplain ViewTable#width} символов
	 */
	private void outHeader() {
		Formatter fmt = new Formatter();
		fmt.format("%s%d%s%2$d%s%s%s%s","%", (width-3)/5, "s | %", "s | %", "s | %",
																	"s | %", "s\n"); 
		System.out.printf(fmt.toString(), "Weight","G  ", "Height", "F	", "F(binary)");
	}
	
	/**
	 * Выводит тело таблицы шириной {@linkplain ViewTable#width} символов
	 */
	private void outBody() {
		Formatter fmt = new Formatter();
		fmt.format("%s%d%s%2$d%s%s%s%s","%", (width-3)/5, ".2f | %", ".2f | %"
									 , ".2f | %",".2f | %",  "s\n");
		for(Item2d item : getItems()) {
			System.out.printf(fmt.toString(), item.getWeight(),item.getG(), item.getHeight(),
					item.getY(), Long.toBinaryString(Double.doubleToRawLongBits(item.getY())));		
		}
	}
	
	/**
	 * Перегрузка метода суперкласса;
	 * устанавливает поле {@linkplain ViewTable#width} 
	 * значением <b>width</b><br>
	 * Вызывает метод {@linkplain ViewResult#ViewInit() ViewInit()}
	 * @param width новая ширина таблицы
	 */
	public final void init(int width) {
		this.width = width;
		viewInit();
	}
	
	/**
	 * Перегрузка методда суперкласса;
	 * устанавливает поле {@linkplain ViewTable#width}
	 * значением <b>width</b><br>
	 * Для объекта {@linkplain ViewTable} вызывает метод
	 * {@linkplain ViewTable#init(double stepW, double stepG, double stepG)}
	 * @param width новая ширина таблицы
	 * @param stepW пеердается методу <b>init(double, double, double)</b>
	 * @param stepG пеердается методу <b>init(double, double, double)</b>
	 * @param stepH пеердается методу <b>init(double, double, double)</b>
	 */
	public final void init(int width, double stepW, double stepG, double stepH) {
		this.width = width;
		init(stepW, stepG, stepH);
	}
	
	/**
	 * Переопределение метода суперкласса;
	 * выводит ифнормационное сообщение и вызывает метод суперкласса
	 * {@linkplain ViewResult#init(double weight, double g, double height) init(double weight, double g, double height)}<br>
	 * {@inheritDoc}
	 */
	@Override
	public void init(double weight, double g, double height) {
		System.out.println("Initialization.............");
		super.init(weight, g, height);
		System.out.println("done. ");
	}
	
	/**
	 * Вывод элемента таблицы <b>{@inheritDoc}
	 */
	@Override
	public void viewHeader() {
		outHeader();
		outLineLn();
	}
	
	/**
	 * Вывод элемента таблицы <b>{@inheritDoc}
	 */
	@Override
	public void viewBody() {
		outBody();
	}
	
	/**
	 * Вывод элемента таблицы <b>{@inheritDoc}
	 */
	@Override
	public void viewFooter() {
		outLineLn();
	}
}
