package ua.khpi.project.lab12;
import ua.khpi.project.lab11.View;
import ua.khpi.project.lab11.Viewable;
import ua.khpi.project.lab11.ViewableResult;

/** ConcreteCreator
 * (шаблон проектирования
 * Factory Method) <br>
 * Объявляет метод
 * "фабрикующий" объекты
 * @author Рудаков И.С., КИТ-27Б
 * @version 1.0
 * @see Viewable
 * @see ViewableResult#getView()
 */

public class ViewableTable extends ViewableResult {
	@Override
	public View getView() {
		return new ViewTable();
	}

}
