package ua.khpi.project.lab12;
import ua.khpi.project.lab11.View;

/**
 * Вычисление и отображение результатов. Содержит реализацию статического метода
 * main().
 * 
 * @author Рудаков И.С., КИТ-27Б
 * @version 3.0
 * @see Main#main
 */
public class Main extends ua.khpi.project.lab11.Main {
	
	/**
	 * Инициализирует поле {@linkplain ua.khpi.project.lab11.Main#view view}
	 */
	public Main(View view) {
		super(view);
	}

	/**
	 * Выполняется при запуске программы;
	 * вызывает метод {@linkplain ua.khpi.project.lab11.Main#menu() menu()}
	 * @param args - параметры запуска программы
	 */
	public static void main(String[] args) {
		Main main = new Main(new ViewableTable().getView());
		main.menu();		
	}

}
