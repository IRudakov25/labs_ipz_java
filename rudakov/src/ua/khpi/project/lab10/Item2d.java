package ua.khpi.project.lab10;

import java.io.Serializable;
/** Хранит исходные данные и результат вычислений.
 * @author Рудаков И.С., КИТ-27Б
 * @version 1.0
 */
public class Item2d implements Serializable {
	/** Аргумент вычисляемой функции. */
	// transient
	private double weight;
	/** Аргумент вычисляемой функции. */
	private double g;
	/** Аргумент вычисляемой функции. */
	private double height;
	/** Результат вычисления функции. */
	private double expression;
	/** Автоматически сгенерированная константа */
	private static final long serialVersionUID = 1L;
	/** Инициализирует поля {@linkplain Item2d#weight}, {@linkplain Item2d#g},
	 * 						{@linkplain Item2d#height},{@linkplain Item2d#expression} */
	public Item2d() {
		weight = .0;
		g = 9.81;
		height = .0;
		expression = .0;
	}
	/** Устанавливает значения полей: аргумента
	 * и результата вычисления функции.
	 * @param m - значение для инициализации поля {@linkplain Item2d#weight}
	 * @param g - значение для инициализации поля {@linkplain Item2d#g}
	 * @param h  - значение для инициализации поля {@linkplain Item2d#height}
	 * @param y - значение для инициализации поля {@linkplain Item2d#expression}
	 */
	public Item2d(double m, double g, double h, double y) {
		this.weight = m;
		this.g = g;
		this.height = h;
		this.expression = y;
	}
	/** Установка значения поля {@linkplain Item2d#weight}
	 * @param m - значение для {@linkplain Item2d#weight}
	 * @return Значение {@linkplain Item2d#x}
	 */
	public double setWeight(double m) {
		return this.weight = m;
	}
	/** Получение значения поля {@linkplain Item2d#weight}
	 * @return Значение {@linkplain Item2d#weight}
	 */
	public double getWeight() {
		return weight;
	}
	/** Установка значения поля {@linkplain Item2d#g}
	 * @param g - значение для {@linkplain Item2d#g}
	 * @return Значение {@linkplain Item2d#g}
	 */
	public double setG(double g) {
		return this.g = g;
	}
	/** Получение значения поля {@linkplain Item2d#g}
	 * @return Значение {@linkplain Item2d#g}
	 */
	public double getG() {
		return g;
	}
	/** Установка значения поля {@linkplain Item2d#height}
	 * @param h - значение для {@linkplain Item2d#height}
	 * @return Значение {@linkplain Item2d#height}
	 */
	public double setHeight(double h) {
		return this.height = h;
	}
	/** Получение значения поля {@linkplain Item2d#height}
	 * @return Значение {@linkplain Item2d#height}
	 */
	public double getHeight() {
		return height;
	}
	/** Установка значения поля {@linkplain Item2d#expression}
	 * @param y - значение для {@linkplain Item2d#expression}
	 * @return Значение {@linkplain Item2d#expression}
	 */
	public double setFormula(double y) {
		return this.expression = y;
	}
	/** Получение значения поля {@linkplain Item2d#expression}
	 * @return значение {@linkplain Item2d#expression}
	 */
	public double getY() {
		return expression;
	}
	/** Установка значений {@linkplain Item2d#weight}, {@linkplain Item2d#g},
	 * 						{@linkplain Item2d#height}, {@linkplain Item2d#expression}
	 * @param m - значение для инициализации поля {@linkplain Item2d#weight}
	 * @param g - значение для инициализации поля {@linkplain Item2d#g}
	 * @param h  - значение для инициализации поля {@linkplain Item2d#height}
	 * @param y - значение для инициализации поля {@linkplain Item2d#expression}
	 * @return this
	 */
	public Item2d setMGHY(double m, double g, double h, double y) {
		this.weight = m;
		this.g = g;
		this.height = h;
		this.expression = y;
		return this;
	}
	/** Представляет результат вычислений в виде строки.<br>{@inheritDoc} */
	@Override
	public String toString() {
		return "F = m*g*h\nWeight = " + weight + " kilos, g = " + g + " (m^2)/sec, height = " + height 
				+ " m.\nF(double) = " + expression
				+ " J.\tF = " + Long.toBinaryString(Double.doubleToRawLongBits(expression)) + " J.";
	}

	/** Автоматически сгенерированный метод.<br>{@inheritDoc} */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Item2d other = (Item2d) obj;
		if (Double.doubleToLongBits(weight) != Double.doubleToLongBits(other.weight))
			return false;
		if (Double.doubleToLongBits(g) != Double.doubleToLongBits(other.g))
			return false;
		if (Double.doubleToLongBits(height) != Double.doubleToLongBits(other.height))
			return false;
		// изменено сравнение результата вычисления функции
		if (Math.abs(Math.abs(expression) - Math.abs(other.expression)) > .1e-10)
			return false;
		return true;
	}
}
