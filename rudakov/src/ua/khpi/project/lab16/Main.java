package ua.khpi.project.lab16;

import java.util.Scanner;

import ua.khpi.project.lab11.View;
import ua.khpi.project.lab11.ViewableResult;
import ua.khpi.project.lab12.ViewableTable;
import ua.khpi.project.lab13.Application;
/** Вычисление и отображение
* результатов; cодержит реализацию
* статического метода main()
* @author Рудаков И.С., группа КИТ-117Г
* @version 7.0
* @see Main#main
*/
public class Main {

    public static void main(String[] args) {
        int menuItem = 0;
        Scanner scanner = new Scanner(System.in);
        String format = "| %-29s |%n";

        do {
            System.out.println("+-------------------------------+");
            System.out.format(format,"Select view mode");
            System.out.println("+-------------------------------+");
            System.out.format(format,"1) Simple view.");
            System.out.format(format,"2) Table view.");
            System.out.format(format,"3) Window view.");
            System.out.format(format,"4) Exit.");
            System.out.println("+-------------------------------+");
            System.out.print(">>> ");
            menuItem = scanner.nextInt();
        } while (menuItem<0||menuItem>3);

        View view=null;

        switch (menuItem) {
            case 1: {
                view=new ViewableResult().getView();
                break;
            }
            case 2: {
                view= new ViewableTable().getView();
                break;
            }
            case 3: {
                view= new ViewableWindow().getView();
                break;
            }
            default:
                break;
        }

        Application application=Application.getInstance();

        application.run(view);

        scanner.close();
    }

}
