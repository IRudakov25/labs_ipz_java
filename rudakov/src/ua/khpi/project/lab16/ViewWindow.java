package ua.khpi.project.lab16;

import java.awt.Dimension;
import ua.khpi.project.lab11.ViewResult;

/**
 * ConcreteProduct (шаблон проектирования Factory Method); отображение графика
 * 
 * @author Рудаков И.С., группа КИТ-117Г
 * @version 1.0
 * @see ViewResult
 * @see Window
 */
public class ViewWindow extends ViewResult {
	/** Количество элементов коллекци */
	private static final int POINTS_NUM = 100;
	/** Отображаемое окно */
	private Window window = null;

	/** Создание и отображение окна */
	public ViewWindow() {
		super(POINTS_NUM);
		window = new Window(this);
		window.setSize(new Dimension(640, 480));
		window.setTitle("Result");
		window.setVisible(true);
		window.setResizable(false);
	}

	@Override
	public void viewInit() {
		init((10.0 + Math.random() * 100.0) / (POINTS_NUM - 1), 9.81,
				(25.0 + Math.random() * 360.0) / (POINTS_NUM - 1));
	}

	@Override
	public void viewShow() {
		super.viewShow();
		window.setVisible(true);
		window.repaint();
	}
}
