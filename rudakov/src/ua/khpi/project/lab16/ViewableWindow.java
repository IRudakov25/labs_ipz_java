package ua.khpi.project.lab16;

import ua.khpi.project.lab11.View;
import ua.khpi.project.lab11.Viewable;

/**
 * ConcreteCreator (шаблон проектирования Factory Method); реализует метод,
 * "фабрикующий" объекты
 * 
 * @author Рудаков И.С., группа КИТ-117Г
 * @version 1.0
 * @see Viewable
 * @see Viewable#getView()
 */
public class ViewableWindow implements Viewable {
	@Override
	public View getView() {
		return new ViewWindow();
	}
}
