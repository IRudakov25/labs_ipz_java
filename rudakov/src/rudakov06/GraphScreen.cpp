/**
 * @file GraphScreen.cpp
 * Part of Lab4
 * @author Rudakov I., KIT-27b
 * @version 0.0.1
 * @date 2019.10.14
 */

#include "GraphScreen.h"

GraphScreen::GraphScreen() {
	// TODO Auto-generated constructor stub

}

GraphScreen::~GraphScreen() {
	// TODO Auto-generated destructor stub
}

void GraphScreen::ShowHeader(){
	std::cout << "\u2554" << "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"
						<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2566" << "\u2550" << "\u2550"
						<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2557\n";
			std::cout << "\u2551" << "Is Button Pressed?" << "\u2551" << "Shape" << "\u2551\n";
			std::cout << "\u2560"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"
						<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2569"<< "\u2550"<< "\u2550"
						<< "\u2550"<< "\u2550" << "\u2550"<< "\u2563\n";
}
void GraphScreen::ShowContent(const Button& iButton){
	std::cout << "\u2551" << "\u0020"<< "\u0020"<< "\u0020"<< "\u0020"<< "\u0020"<< "\u0020"<< "\u0020"<<  "\u0020"<< "\u0020"
					<< iButton.getButton()<< "\u0020" << "\u0020"<< "\u0020"<< "\u0020"<< "\u0020"<< "\u0020"<< "\u0020" << "\u0020" << "\u2551"
					<< "\u0020"<< "\u0020"<< iButton.getShape()<<  "\u0020"<< "\u0020"<< "\u2551\n";
	std::cout << "\u2560"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"
					<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"
					<< "\u2550" << "\u2550" << "\u2550"<< "\u2563\n";

}
void GraphScreen::ShowContent(const KeyboardButton& iKeyboardButton){}
void GraphScreen::ShowFooter(){

	std::cout << "\u2551" << "\u0020"<< "\u0020"<< "\u0020"<< "\u0020"
					<< "\u0020" << "\u0020"<< "\u0020"<< "THIS IS END" << "\u0020"
					<< "\u0020"<< "\u0020"<<  "\u0020"<< "\u0020"<< "\u0020"<< "\u2551\n";
	std::cout << "\u255A" << "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"
						<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550"<< "\u2550" << "\u2550" << "\u2550"
						<< "\u2550"<< "\u2550"<< "\u2550"<< "\u255D\n";
}

