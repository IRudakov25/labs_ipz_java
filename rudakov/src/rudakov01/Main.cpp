/**
 * @file Main.cpp
 * Part of Lab1
 * @author Rudakov I., KIT-27b
 * @version 0.0.1
 * @date 2019.09.27
 */

#include "Button.h"
#include "Screen.h"


int main(){
	Button *button = new Button;
	/**
	 * You can choose a condition.
	 * true - the button is pressed
	 * false - the button is unpressed
	 */
	button->isButtonPressed = true;
	/**
	 * You can choose a shape.
	 * RECTANGULAR - rectangular
	 * OVAL - oval
	*/
	button->shape = OVAL;
	Screen *screen = new Screen;
	std::cout << "First Button" << std::endl;
	screen->toPrint(button);

	Button *buttonSecond = new Button;
	buttonSecond->isButtonPressed = false;
	buttonSecond->shape = RECTANGULAR;
	Screen *screenSecond = new Screen;
	std::cout << "Second Button" << std::endl;
	screenSecond->toPrint(buttonSecond);
	delete button;
	delete screen;
	delete buttonSecond;
	delete screenSecond;
	return 0;
}
