/**
 * @file Button.h
 * Part of Lab1
 * @author Rudakov I., KIT-27b
 * @version 0.0.1
 * @date 2019.09.27
 */

#ifndef BUTTON_H_
#define BUTTON_H_
#include <iostream>

enum Shape{
	RECTANGULAR = 1,
	OVAL
};

class Button {
public:
	Button();
	virtual ~Button();
	Button(const Button &other);
	Button(Button &&other);
	Button(bool isButtonDown, Shape shape1);

	Shape shape;
	bool isButtonPressed;

};

#endif /* BUTTON_H_ */
