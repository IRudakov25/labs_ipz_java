/**
 * @file Screen.h
 * Part of Lab1
 * @author Rudakov I., KIT-27b
 * @version 0.0.1
 * @date 2019.09.27
 */

#ifndef SCREEN_H_
#define SCREEN_H_
#include "Button.h"
#include <iostream>

class Screen {
public:
	Screen();
	virtual ~Screen();
	Screen(const Screen &other);
	Screen(Screen &&other);
	//Button *button;
	void toPrint(Button* button);
};

#endif /* SCREEN_H_ */
