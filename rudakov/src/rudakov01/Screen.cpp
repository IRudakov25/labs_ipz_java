/**
 * @file Screen.cpp
 * Part of Lab1
 * @author Rudakov I., KIT-27b
 * @version 0.0.1
 * @date 2019.09.27
 */

#include "Screen.h"

Screen::Screen() {
	// TODO Auto-generated constructor stub

}

Screen::~Screen() {
	// TODO Auto-generated destructor stub
}

Screen::Screen(const Screen &other) {
	// TODO Auto-generated constructor stub

}

Screen::Screen(Screen &&other) {
	// TODO Auto-generated constructor stub

}

void Screen::toPrint(Button* button){
	if (button->isButtonPressed == 0){
	std::cout << button->isButtonPressed << " - this means that button is unpressed" << std::endl;
	}else{
	std::cout << button->isButtonPressed << " - this means that button is pressed" << std::endl;
	}
	if (button->shape == RECTANGULAR){
		std::cout << button->shape << " - this means that button has rectangular shape" << std::endl;
	}else if (button->shape == OVAL){
		std::cout << button->shape << " - this means that button has oval shape" << std::endl;
	}
}
