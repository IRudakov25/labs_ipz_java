/**
 * @file KeyboardButton.cpp
 * Part of Lab4
 * @author Rudakov I., KIT-27b
 * @version 0.0.1
 * @date 2019.10.14
 */

#include "KeyboardButton.h"

KeyboardButton::KeyboardButton() {
	// TODO Auto-generated constructor stub

}

KeyboardButton::~KeyboardButton() {
	// TODO Auto-generated destructor stub
}

void KeyboardButton::setSerialNumber(int aSerialNumber){
	serialNumber = aSerialNumber;
}
void KeyboardButton::setButtonName(std::string sButtonName){
	buttonName = sButtonName;
}


int KeyboardButton::getSerialNumber() const{
	return serialNumber;
}
std::string KeyboardButton::getButtonName() const{
	return buttonName;
}

void KeyboardButton::SetData(const int& serNum){
	this->setSerialNumber(serNum);
	std::cout << "SetData(const int&) " << this->getSerialNumber() << std::endl;
}

void KeyboardButton::SetData(const int& serNum, const std::string& butName){
	this->setSerialNumber(serNum);
	this->setButtonName(butName);
	std::cout << "SetData(const int&, const std::string&) " << this->getSerialNumber() << ",\t" << this->getButtonName() << std::endl;
}

bool KeyboardButton::operator==(int serName){
	if(this->getSerialNumber() == serName){
		return true;
	}else{
		return false;
	}
}

