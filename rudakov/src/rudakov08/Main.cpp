/**
 * @file Main.cpp
 * Part of Lab8
 * @author Rudakov I., KIT-27b
 * @version 0.0.1
 * @date 2019.10.14
 */

#include "Button.h"
#include "Screen.h"
#include "GraphScreen.h"
#include "KeyboardButton.h"
#include "BaseView.h"
#include "KeyboardButtonScreen.h"
#include "FileStorage.h"
#include "ButtonList.h"

int main(){
	Button button;
	ButtonList list;

		Button *buttonOvPres = new Button();
		buttonOvPres->setButton(true);
		buttonOvPres->setShape(OVAL);

		list.addButton(buttonOvPres);
		list.showAll();
		std::cout<< list[10]->getButton() << std::endl;

		delete buttonOvPres;

	return 0;
}
